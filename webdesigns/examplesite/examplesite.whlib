<?wh

LOADLIB "wh::datetime.whlib";
LOADLIB "wh::internet/urls.whlib";

LOADLIB "mod::publisher/lib/publisher.whlib";
LOADLIB "mod::publisher/lib/webdesign.whlib";

LOADLIB "mod::system/lib/cache.whlib";
LOADLIB "mod::system/lib/database.whlib";
LOADLIB "mod::system/lib/whfs.whlib";

LOADLIB "mod::examples/webdesigns/examplesite/shared/support.whlib";


PUBLIC OBJECTTYPE ExampleSiteDesign EXTEND WebDesignBase
<
  RECORD pagesettings;
  INTEGER newsfoldertypeid;

  MACRO NEW()
  {
    //Set some central object parameters so these can be directly used in the object functions

    //Get pagesettings defined in siteprofile for current file
    this->pagesettings := this->targetobject->GetInstanceData("http://examplesite.webhare.com/xmlns/page");

    //Get news folder type id for checking folder type in queries
    this->newsfoldertypeid := OpenWHFSType("http://examplesite.webhare.com/xmlns/folders/news")->id;
  }

  UPDATE PUBLIC MACRO PrepareErrorPage(INTEGER errorcode, RECORD harescriptinfo, STRING url)
  {
    IF( errorcode IN [404] )
    {
      this->pagetitle := ToString(errorcode) || " - " || this->siteconfig.sitetitle; //Set page/SEO title
      this->pageconfig.title := ToString(errorcode); //used for heading1
      this->contentbody := PTR Print("<p>Page not found</p>"); // [contents]
    }
    ELSE
      WebDesignBase::PrepareErrorPage(errorcode,harescriptinfo,url);
  }

  // Get data for site which don't depends on the separate pages
  //  (selection for navigation will be done afterwards)
  UPDATE RECORD FUNCTION GetCacheableSiteConfig()
  {
    RECORD sitesettings := this->targetsite->rootobject->GetInstanceData("http://examplesite.webhare.com/xmlns/site");

    RETURN [ mainnav      := this->GetMainNav()
           , footernav    := (SELECT title, link := GetIntextLinkTarget(link) FROM sitesettings.footerlinks)
           , sitesettings := sitesettings
           , sitetitle    := this->targetsite->rootobject->title ?? this->targetsite->rootobject->name
           , socialmedia  := (SELECT *, icon := GetNetworkIcon(network) FROM sitesettings.socialmedia)
           ];
  }

  RECORD ARRAY FUNCTION SetSelectionInItemsTree( RECORD ARRAY allitems, STRING activewhfspath)
  {
    RETURN
      SELECT *
           , isselected := activewhfspath LIKE allitems.whfspath || "*"
           , items := this->SetSelectionInItemsTree(items, activewhfspath)
        FROM allitems;
  }

  UPDATE PUBLIC RECORD FUNCTION GetPageConfig()
  {
    //Set selection in navigation for the cached main navigation items
    RECORD ARRAY mainnav := this->SetSelectionInItemsTree(this->siteconfig.mainnav, this->targetobject->whfspath);

    this->SetSEOTitle();
    this->SetShareImage();

    BOOLEAN isindex := this->targetfolder->indexdoc = this->targetobject->id;
    BOOLEAN isnewsfolder := this->newsfoldertypeid = this->targetfolder->type;

    RETURN [ sitetitle     := this->siteconfig.sitetitle
           , title         := this->targetobject->title
           , mainnav       := mainnav
           , footercols    := this->GetFooterCols()
           , footernav     := this->siteconfig.footernav
           , contentlist   := isindex AND isnewsfolder ? this->GetContentList() : DEFAULT RECORD ARRAY
           , socialmedia   := this->siteconfig.socialmedia
           , pathnav       := this->GetPathNav()
           , dateformatted := isnewsfolder AND NOT isindex ? FormatDateTime("%d %B %Y", this->pagesettings.date, this->languagecode ) : ""
           , headerimage   := this->GetHeaderImage()
           , currentyear   := FormatDateTime("%Y", UTCToLocal(GetCurrentDateTime(),"CET"))
           ];
  }

  RECORD FUNCTION GetFooterCols()
  { /* gather data for the footer columns in one record
      1: logo (already set in witty template)
      2: Title + Quicklinks
      3: Title + RTD
      4: Title + RTD
    */
    RETURN [ quicklinks_title := this->siteconfig.sitesettings.quicklinks_title
           , quicklinks       := (SELECT title, link := GetIntextLinkTarget(link) FROM this->siteconfig.sitesettings.quicklinks)
           , column3_title    := this->siteconfig.sitesettings.footercol3_title
           , column3_content  := PTR this->OpenRTD(this->siteconfig.sitesettings.footercol3_content)->RenderAllObjects
           , column4_title    := this->siteconfig.sitesettings.footercol4_title
           , column4_content  := PTR this->OpenRTD(this->siteconfig.sitesettings.footercol4_content)->RenderAllObjects
           ];
  }

  MACRO SetShareImage()
  { // If page has a header image, override the default share image defined in siteprofile (og:image)
    IF( RecordExists(this->pagesettings.headerimage) )
    {
      STRING shareimage := GetCachedImageLink( this->pagesettings.headerimage, [ method := "fill", setwidth := 1200, setheight := 630 ] );
      shareimage := ResolveToAbsoluteURL(this->targetsite->webroot, shareimage);
      this->GetPlugin("http://www.webhare.net/xmlns/socialite","opengraph")->ogproperties.image := shareimage;
    }
  }

  MACRO SetSEOTitle()
  { // Set html/browser-tab/window title (this->pagetitle)
    // if seotitle not set, display 'pagetitle - sitetitle'
    IF( this->pagesettings.seotitle != "" )
      this->pagetitle := this->pagesettings.seotitle;
    ELSE
    {
      this->pagetitle := this->siteconfig.sitetitle;
      IF( this->targetobject->title != "" ) //If page has title, add this before site title
        this->pagetitle := this->targetobject->title || " - " || this->pagetitle;
      ELSE IF( this->targetfolder->id != this->targetsite->root AND this->targetfolder->title != "" ) //Use folder title if not siteroot and no pagetitle is set
        this->pagetitle := this->targetfolder->title || " - " || this->pagetitle;
    }
  }

  RECORD FUNCTION GetHeaderImage()
  {
    RECORD image;
    IF( RecordExists(this->pagesettings.headerimage) )
    { /* Aspect 33.3333%
         Generate different images sizes for different window sizes.
         Image to diplayed image is set by media query in witty template
         wrapcachedimage returns:
           width
           height
           link
           dominantcolor
           refpoint_backgroundposition (empty if not set)
      */
      image := [ img2880 := WrapCachedImage( this->pagesettings.headerimage, [ method    := "fill"
                                                                             , setwidth  := 2880
                                                                             , setheight := 960
                                                                             ])
               , img1440 := WrapCachedImage( this->pagesettings.headerimage, [ method    := "fill"
                                                                             , setwidth  := 1440
                                                                             , setheight := 480
                                                                             ])
               , img720  := WrapCachedImage( this->pagesettings.headerimage, [ method    := "fill"
                                                                             , setwidth  := 720
                                                                             , setheight := 240
                                                                             ])
               ];
    }

    RETURN image;
  }

  RECORD ARRAY FUNCTION GetMainNav()
  { //Get all folders with title in siteroot and items in these folders
    RETURN (SELECT *
                 , items := this->GetFolderItems(id, TRUE, type != this->newsfoldertypeid)
              FROM this->GetFolderItems( this->targetsite->root, TRUE, FALSE ));
  }

  RECORD ARRAY FUNCTION GetFolderItems( INTEGER folderid, BOOLEAN showfolders, BOOLEAN showfiles )
  { //get items in given folder
    INTEGER folderindex := SELECT AS INTEGER indexdoc FROM system.fs_objects WHERE id = folderid;
    RETURN (SELECT *
                 , items := DEFAULT RECORD ARRAY //placeholder for use of subitems
              FROM system.fs_objects
             WHERE parent = folderid // Items in folder
               AND link != ""        // Published pages
               AND title != ""       // Must have title
               AND id != folderindex //Ignore indexfile of folder
               AND ((isfolder AND showfolders) OR (NOT isfolder AND showfiles))
          ORDER BY ordering, ToUpperCase(title), name);
  }

  RECORD ARRAY FUNCTION GetContentList()
  {
    RECORD ARRAY items := SELECT * FROM this->GetFolderItems( this->targetfolder->id, FALSE, TRUE );

    BOOLEAN isnewsfolder := this->targetfolder->type = this->newsfoldertypeid;

    //Generate thumbs from headerimage and format date if in news folder
    items := OpenWHFSType("http://examplesite.webhare.com/xmlns/page")->Enrich(items, "id", ["date", "headerimage"]);
    items := SELECT *
                  , image := RecordExists(headerimage) ? [ normal := WrapCachedImage( headerimage, [ method    := "fill"
                                                                       , setwidth  := 160
                                                                       , setheight := 90
                                                                       ])
                                                         , retina := WrapCachedImage( headerimage, [ method    := "fill"
                                                                                                   , setwidth  := 320
                                                                                                   , setheight := 180
                                                                                                   ])
                                                         ]
                                                       : DEFAULT RECORD
                  , dateformatted := isnewsfolder ? FormatDateTime("%d %B %Y", date, this->languagecode ) : ""
               FROM items;

    IF( isnewsfolder )
      items := SELECT * FROM items ORDER BY date DESC, #items;//first date then orginal order

    RETURN items;
  }

  RECORD ARRAY FUNCTION GetPathNav()
  { //Get all ids from site root until current folder
    // First item is the site root, last the current folder id
    INTEGER ARRAY foldertree_ids := GetFolderTreeIDs(this->targetfolder->id);

    RECORD ARRAY pathnav := SELECT *, title := title ?? name
                              FROM system.fs_objects
                             WHERE id IN foldertree_ids
                               AND link != ""  // only with published index
                               AND isfolder
                          ORDER BY SearchElement(foldertree_ids, id); //Keep order set in foldertree_ids

    IF( Length(pathnav) > 0 )
    {
      pathnav[0].title := "Home"; //Overwrite first title (home)

      //If current file is not the index, add current file to pathnav
      IF( this->targetobject->id != this->targetfolder->indexdoc AND this->targetfolder->id != this->targetobject->id /* 404 page folder/object id are same */)
      {
        INSERT [ link  := this->targetobject->link
               , title := this->targetobject->title ?? this->targetobject->name
               ] INTO pathnav AT END;
      }

      IF( Length(pathnav) = 1 )
        RETURN DEFAULT RECORD ARRAY;//If just one item (home), then don't show pathnav
    }

    RETURN pathnav;
  }
>;
