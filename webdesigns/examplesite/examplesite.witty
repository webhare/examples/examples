[rawcomponent signature]
<!--
Realisatie: WebHare bv
            https://www.webhare.nl/
-->
[/rawcomponent]

[component htmlhead]
  [embed signature]
  <meta name="viewport" content="width=device-width, initial-scale=1" />

  <link rel="apple-touch-icon" sizes="180x180" href="[imgroot]favicons/apple-touch-icon.png"/>
  <link rel="icon" type="image/png" sizes="32x32" href="[imgroot]favicons/favicon-32x32.png"/>
  <link rel="icon" type="image/png" sizes="16x16" href="[imgroot]favicons/favicon-16x16.png"/>
  <link rel="manifest" href="[imgroot]favicons/manifest.json"/>
  <link rel="mask-icon" href="[imgroot]favicons/safari-pinned-tab.svg" color="#008fd1"/>
  <link rel="shortcut icon" href="[imgroot]favicons/favicon.ico"/>
  <meta name="apple-mobile-web-app-title" content="[sitetitle]"/>
  <meta name="application-name" content="[sitetitle]"/>
  <meta name="msapplication-config" content="[imgroot]favicons/browserconfig.xml"/>
  <meta name="theme-color" content="#ffffff"/>

  [if ishomepage]
    <script type="application/ld+json">
    {"@context": "http://schema.org"
    ,"@type": "WebSite"
    ,"url": "[siteroot]"
    ,"potentialAction": {"@type": "SearchAction"
                        ,"target": "[siteroot]search/?words={query}"
                        ,"name": "words"
                        ,"query-input": "required name=query"
                        }
    }
    </script>
  [/if]
[/component]

[component htmlbody]
  <header id="mainnav">
    <nav class="centercontent">
      <a href="[siteroot]" class="logo">
        <img src="[imgroot]webhare-icon.svg" alt="[sitetitle]" />
        <span class="highlight">Web</span>Hare
      </a>

      <div tabindex="0" class="toggle-topsearch">
        <i class="search fa fa-search"></i>
      </div>

      <div tabindex="0" class="mobilenav-toggle">
        <i class="mobilenav-toggle__show fas fa-bars"></i>
        <i class="mobilenav-toggle__hide fas fa-times"></i>
      </div>

      <ul class="menuitems">
        <li [if ishomepage]class="active"[/if]>
          <a href="[siteroot]"><span class="title">Home</span></a>
        </li>
        [forevery mainnav]
          <li [if isselected]class="active"[/if]>
            <a href="[link]">
              <span class="title">[title]</span>
              [if items]<i class="moreitems fa fa-chevron-down"></i>[/if]
            </a>
            [if items]
              <ul>
                [forevery items]
                  <li [if isselected]class="active"[/if]>
                    <a href="[link]">[title]</a>
                  </li>
                [/forevery]
              </ul>
            [/if]
          </li>
        [/forevery]
      </ul>
    </nav>

    [embed ./pages/search/search.witty:topsearch]
  </header>

  [if headerimage]
    <header class="headerimage">
      <picture style="background-color:[img720.dominantcolor];
                      [if img720.refpoint_backgroundposition]object-position:[img720.refpoint_backgroundposition];[/if]
                     ">
        <source srcset="[img2880.link]" media="(min-width: 1440px)">
        <source srcset="[img1440.link]" media="(min-width: 720px)">
        <img src="[img720.link]" alt="" />
      </picture>
      <div class="content">
        [if dateformatted]
          <div class="news-date"><i class="far fa-clock"></i> [dateformatted]</div>
        [/if]
        [if title]
          <h1 class="heading1">[title]</h1>
        [/if]
      </div>
    </header>
  [/if]

  <main>
    [if pathnav]
      <ol id="pathnav" class="centercontent">
        [forevery pathnav]
          <li><a href="[link]">[title]</a></li>
        [/forevery]
      </ol>
    [/if]

    [if not headerimage]
      [if dateformatted]
        <div class="news-date"><i class="far fa-clock"></i> [dateformatted]</div>
      [/if]
      [if title]
        <h1 class="heading1">[title]</h1>
      [/if]
    [/if]

    <!--wh_consilio_content-->
    [contents]
    <!--/wh_consilio_content-->

    [if contentlist]
      <div class="contentlist">
        <ul>
          [forevery contentlist]
            <li>
              <a href="[link]">
                <span class="image" [if image]style="background-color:[normal.dominantcolor];"[/if]>
                  [if image]
                    <img src="[normal.link]" srcset="[normal.link] 1x, [retina.link] 2x" alt="" />
                  [/if]
                </span>
                [if dateformatted]
                  <span class="date"><i class="far fa-clock"></i> [dateformatted]</span>
                [/if]
                <span class="title">[title]</span>
                <span class="description">[description]</span>
              </a>
            </li>
          [/forevery]
        </ul>
      </div>
    [/if]
  </main>

  [if ishomepage]
    <div class="newslettersubscription">
      <form class="centercontent" action="#" method="post">
        <div class="title">Want to stay up to date?</div>
        <input type="email" name="email" placeholder="Email" />
        <button type="submit">Send</button>
      </form>
    </div>
  [/if]

  <footer>
    <div class="centercontent">
      [if footercols]
        <div class="columns">
          <div class="col col--logo">
            <a href="[siteroot]" class="logo">
              <img src="[imgroot]webhare-icon-white.svg" alt="[sitetitle]" />
              WebHare
            </a>
          </div>
          <div class="col [if quicklinks]col--quicklinks[/if]">
            [if quicklinks]
              <div class="col__title">
                [if quicklinks_title]
                  [quicklinks_title]
                [else]
                  Quicklinks
                [/if]
              </div>
              <div class="col__content">
                <ul>
                  [forevery quicklinks]
                    <li class="line"><a href="[link]">[title]</a></li>
                  [/forevery]
                </ul>
              </div>
            [/if]
          </div>
          <div class="col col--address">
            <div class="col__title">[column3_title]</div>
            <div class="col__content">
              [column3_content]
            </div>
          </div>
          <div class="col col--contact">
            <div class="col__title">[column4_title]</div>
            <div class="col__content">
              [column4_content]
            </div>
          </div>
        </div>
      [/if]

      <nav id="footernav">
        <ul class="footernav">
          <li>&copy; [currentyear] WebHare Application Portal</li>
          [forevery footernav]
            <li><a href="[link]">[title]</a></li>
          [/forevery]
        </ul>

        [if socialmedia]
          <ul class="socialmedia">
            [forevery socialmedia]
              <li><a href="[link]" title="[network]"><i class="icon [icon]"></i></a></li>
            [/forevery]
          </ul>
        [/if]
      </nav>
    </div>
  </footer>

  <div class="rpc__busyoverlay"><i class="spinner fas fa-circle-notch fa-spin"></i></div>
[/component]
