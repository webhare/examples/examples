import "./debug.scss";
import * as dompack from "dompack";
import * as cookie from "dompack/extra/cookie";

dompack.onDomReady(() =>
{ //show overlay with column grid
  let cols = -1;

  //get nr of cols from url parameter 'cols' if given
  const params = new URLSearchParams(location.search);
  if (params.has("cols"))
    cols = 1 * params.get("cols");

  if (cols > -1)
    cookie.write("debuggrid", cols);
  else
    cols = 1*(cookie.read("debuggrid"));

  if (!cols || cols < 0)
    return;

  let container = <div class="layoutoverlay" />;
  let columnsnode = <div class={"layoutoverlay__columns layoutoverlay__columns--cols" + cols } />;
  container.appendChild(columnsnode);
  for (let i = 0; i < cols; ++i)
  {
    if (i > 0)
      columnsnode.appendChild( <div class="gap" /> );
    columnsnode.appendChild( <div class="col" /> );
  }

  document.body.appendChild(container);
});
