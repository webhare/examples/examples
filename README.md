# WebHare example site webdesign

This is the example site webdesign. See https://www.webhare.dev/guides/getting-started/
for more information.

To install this module, run
```
wh module get https://gitlab.com/webhare/examples/examples.git
```
